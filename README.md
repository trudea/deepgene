# DeepGene - Gene finding based on upstream sequence data

Genome annotation is a process of identifying functional elements along a genome. By
correctly locating and finding the information stored within a sequence, knowledge about
structural features and functional roles can be revealed. With the number of sequences
doubling approximately every 18 months, there is a severe need for automatic annotation of
genomes. Today there are many different annotation software tools available, however they
produce far from perfect results.

Here a new project, DeepGene, is presented. Using data from the RefSeq prokaryotic database
we have started an effort to improve on the prokaryotic genome annotation process.
This thesis presents the initial efforts of said improvement with a focus on discerning between
coding and non-coding sequences using upstream sequence data from open reading frames.
Using the 15 prokaryotic genomes available in the RefSeq database, upstream data was
retrieved and processed into two datasets, and were then trained using several popular
classification models. The performance of the models was compared with a standard
annotation tool to create a general baseline for our model. The models created from the
datasets show many similarities in terms of metrics. With the K-mer data having a mean
precision at 0.22 and mean recall of 0.74, and the sequential data having a mean precision at
0.30 and mean recall at 0.77. Both the datasets performed worse than our standard annotation
software with a mean recall and precision of, respectively, 0.83 and 0.82. As far as upstream
sequences are concerned, the models managed to pull all the information available from both
datasets. The initial results gave limited information in terms of classification and motif
presence indicating that other attributes surrounding the genome should be looked at for a
possible improvement on the annotation problem. An ideal step forward is to expand into a
pipeline so that the complex false negative classifications may be explained.


