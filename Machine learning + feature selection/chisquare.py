# -*- coding: utf-8 -*-
"""
Created on Mon Mar 28 18:33:26 2022

@author: Trude
"""

import pandas as pd
from sklearn.feature_selection import chi2
from sklearn.feature_selection import SelectKBest


#%%
data = pd.read_csv("D:\Master\K_mer_data\E.coliO156_L_90.csv", index_col = 0)
#%%
X = data.drop("Target", axis = 1)  # We only want K-mer data
X = X.drop("Length", axis=1)
y = data["Target"] # Selects target variable

#%%

# k = 10 tells four top features to be selected
# Score function Chi2 tells the feature to be selected using Chi Square
test = SelectKBest(score_func=chi2, k=150)
fit = test.fit_transform(X, y)
fit.scores_

#%% The final data
# Get columns to keep and create new dataframe with those only
cols = test.get_support(indices=True)
features_df_new = X.iloc[:,cols]
#%%

from sklearn.feature_selection import chi2
chi_scores = chi2(X,y)

#%%
p_values = pd.Series(chi_scores[1],index = X.columns)
p_values.sort_values(ascending = True , inplace = True)
#%%
p_values[:20].plot.bar()

#%%
scores = pd.Series(chi_scores[0],index = X.columns)
scores.sort_values(ascending = False , inplace = True)


#%%
scores[:20].plot.bar()