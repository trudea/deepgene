# -*- coding: utf-8 -*-
"""
Created on Thu Mar 24 13:05:58 2022

@author: Trude
"""

# Code for creating one hot encoded feature matrix from the upstream
# table

import pandas as pd
import numpy as np

#%%
data1 = pd.read_csv("D:/Master/Upstream_table_L_90.csv", index_col = 0)
#%%
df_ecoli = data1[data1['Species'] == 'E._coli_O156H7_sakai']

#%% Convert to one hot encoded sequence with order
byposition = df_ecoli['Sequence'].apply(lambda x:pd.Series(list(x)))


res = pd.get_dummies(byposition)

#%%

from sklearn.model_selection import train_test_split
X = pd.DataFrame(res)
y = df_ecoli["Target"]
X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, random_state=42)


#%%
from sklearn.ensemble import RandomForestClassifier
import matplotlib.pyplot as plt
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline



pipe = Pipeline([
    ("clf", RandomForestClassifier())
])

n = [50, 60, 70, 80, 90, 100]
md = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
param_grid = {
    "clf__n_estimators": [500, 600, 700, 800, 900, 1000],
    "clf__max_depth": [10, 25],
    "clf__max_features": ["auto"],
}
#%%
# Perform grid search, fit it, and print score
gs = GridSearchCV(pipe, param_grid=param_grid, cv=3, n_jobs=1, verbose=1000)
gs.fit(X_train, y_train)

#%%
print(gs.score(X_train,y_train))
print(gs.score(X_test, y_test)) 
print(gs.best_params_)

#%%
from sklearn.metrics import roc_auc_score
from sklearn import metrics
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

pred = gs.predict(X_test)
target_names = ['No gene', 'Gene']
print(classification_report(y_test, rf_pred, target_names=target_names))
print(confusion_matrix(y_test, rf_pred))


#%%
# random forest with random undersampling for imbalanced classification
# Best estimator at 700 estrimators, 40 depth and 10 min sample split
from numpy import mean
from sklearn.datasets import make_classification
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import RepeatedStratifiedKFold
from imblearn.ensemble import BalancedRandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression

# Create first pipeline for base without reducing features.

pipe = Pipeline([('classifier', LogisticRegression())])


# define model

param_grid = [
    {'classifier' : [BalancedRandomForestClassifier()],
     'classifier__n_estimators' : [500, 600, 700, 800, 900, 1000],
    'classifier__max_depth' : [10, 25],
    'classifier__min_samples_split' : [10]}]

# Create grid search object

clf = GridSearchCV(pipe, param_grid = param_grid,
                   cv = 8, verbose=True, n_jobs=1)

# Fit on data

best_clf = clf.fit(X_train, y_train)

rf_pred = clf.predict(X_test)


#%%
# generate a no skill prediction (majority class)
ns_probs = [0 for _ in range(len(y_test))]

# calculate roc curve

# calculate scores
ns_auc = roc_auc_score(y_test, ns_probs)
lr_auc = roc_auc_score(y_test, pred)
rf_auc = roc_auc_score(y_test, rf_pred)
# summarize scores
print('No Skill: ROC AUC=%.3f' % (ns_auc))
print('Random forest: ROC AUC=%.3f' % (lr_auc))
print('Random forest: ROC AUC=%.3f' % (rf_auc))
# calculate roc curves
ns_fpr, ns_tpr, _ = metrics.roc_curve(y_test, ns_probs)
lr_fpr, lr_tpr, _ = metrics.roc_curve(y_test, pred)
rf_fpr, rf_tpr, _ = metrics.roc_curve(y_test, rf_pred)
# plot the roc curve for the model
plt.plot(ns_fpr, ns_tpr, linestyle='--', label='Random chances')
plt.plot(lr_fpr, lr_tpr, marker='.', label='Random forest ROC AUC=%.3f' % (lr_auc))
plt.plot(rf_fpr, rf_tpr, marker='.', label='Random forest imblearn ROC AUC=%.3f' % (rf_auc))
# axis labels
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
# show the legend
plt.legend()
# show the plot
plt.show()
#%%
#%%
# random forest with random undersampling for imbalanced classification
# Best estimator at 700 estrimators, 40 depth and 10 min sample split

from imblearn.ensemble import BalancedRandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler

# Create first pipeline for base without reducing features.

pipe = Pipeline([('scaler', StandardScaler()),
                 ('classifier', LogisticRegression())])


# define model

param_grid = [
    {'classifier' : [BalancedRandomForestClassifier()],
     'classifier__n_estimators' : [700],
    'classifier__max_depth' : [30, 40],
    'classifier__min_samples_split' : [10]}]

# Create grid search object

clf = GridSearchCV(pipe, param_grid = param_grid,
                   cv = 3, verbose=True, n_jobs=1)

# Fit on data

best_clf = clf.fit(X_train, y_train)

best_clf.best_score_
best_clf.best_estimator_
#%%
# Import the classifiers
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.naive_bayes import BernoulliNB
from sklearn.metrics import roc_curve, roc_auc_score
from imblearn.ensemble import BalancedRandomForestClassifier

# Instantiate the classfiers and make a list
classifiers = [LogisticRegression(random_state=1234, C=0.0006951927961775605, 
                                  class_weight=None,
                                    dual=False, fit_intercept=True,
                                    intercept_scaling=1, l1_ratio=None,
                                    max_iter=1000, multi_class='auto',
                                    n_jobs=None, penalty='l2',
                                    solver='lbfgs',
                                    tol=0.0001, verbose=0, warm_start=False), 
               CategoricalNB(), 
               KNeighborsClassifier(), 
               DecisionTreeClassifier(random_state=1234),
               RandomForestClassifier(random_state=1234,
                                      max_depth=100, 
                                      n_estimators = 1000),
               BalancedRandomForestClassifier(random_state=1234,
                                      max_depth=100, 
                                      n_estimators = 1000)]

# Define a result table as a DataFrame
result_table = pd.DataFrame(columns=['classifiers', 'fpr','tpr','auc'])

# Train the models and record the results
for cls in classifiers:
    model = cls.fit(X_train, y_train)
    yproba = model.predict_proba(X_test)[::,1]
        
    fpr, tpr, _ = roc_curve(y_test,  yproba)
    auc = roc_auc_score(y_test, yproba)
    
    result_table = result_table.append({'classifiers':cls.__class__.__name__,
                                        'fpr':fpr, 
                                        'tpr':tpr, 
                                        'auc':auc,
                                        'preds': yproba}, ignore_index=True)

# Set name of the classifiers as index labels
result_table.set_index('classifiers', inplace=True)

#%% LDA + PLS
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.cross_decomposition import PLSRegression

pipel = Pipeline([('scaler', StandardScaler()),
                  ('red', LinearDiscriminantAnalysis()),
                  ('classifier', PLSRegression())])
    
param_grid_l = [
    {'red__n_components': [1]}
    ]


clfl = GridSearchCV(pipel, param_grid = param_grid_l,
                   cv = 3, verbose=True, n_jobs=1)

# Fit on data

best_clfl = clfl.fit(X_train, y_train)

best_clfl.best_score_
best_clfl.best_estimator_

#%%

y_hat = pd.read_csv("D:/Master/LDA/yhat/E._coli_O156H7_sakai0.csv")
y_tr = pd.read_csv("D:/Master/LDA/y/E._coli_O156H7_sakai0.csv")

fpr, tpr, _ = roc_curve(y_tr["x"],  y_hat["x"])
auc = roc_auc_score(y_tr["x"], y_hat["x"])


#%% Plot
import matplotlib.pyplot as plt

order = [5,4,0,1,2,3,6]
fig = plt.figure(figsize=(8,6))

for i in result_table.index:
    plt.plot(result_table.loc[i]['fpr'], 
             result_table.loc[i]['tpr'], 
             label="{}, AUC={:.3f}".format(i, result_table.loc[i]['auc']))
    
plt.plot(fpr, tpr, label = "PLS + LDA, AUC = {:.3f}".format(auc))
handles, labels = plt.gca().get_legend_handles_labels()
    
plt.plot([0,1], [0,1], color='orange', linestyle='--')

plt.xticks(np.arange(0.0, 1.1, step=0.1))
plt.xlabel("False Positive Rate", fontsize=15)

plt.yticks(np.arange(0.0, 1.1, step=0.1))
plt.ylabel("True Positive Rate", fontsize=15)

plt.title('ROC Curve Analysis for one-hot encoded data', fontweight='bold', fontsize=12)
plt.legend([handles[idx] for idx in order],[labels[idx] for idx in order],
           prop={'size':10}, loc='lower right')

plt.show()
#%%
plt.plot([0,1], [0,1], color='black', linestyle='--')

plt.xticks(np.arange(0.0, 1.1, step=0.1))
plt.xlabel("False Positive Rate", fontsize=10)

plt.yticks(np.arange(0.0, 1.1, step=0.1))
plt.ylabel("True Positive Rate", fontsize=10)
plt.plot(0.8, 0.1, "ko")
plt.plot(0, 1.0, "ko")
plt.plot(0.1, 0.7, "ko")
plt.plot(0.5, 0.5, "ko")

plt.text(0.82, 0.1,'D')
plt.text(0.51, 0.55,'C')
plt.text(0.13, 0.7,'B')
plt.text(0.03, 1.0,'A')

plt.title('ROC', fontweight='bold', fontsize=12)


plt.show()

