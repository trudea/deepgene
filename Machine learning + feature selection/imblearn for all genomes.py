# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 11:16:24 2022

@author: Trude
"""
# This is code run for all genome k-mer data in RefSeq
# Requirement: run k-mer creator in R and create the respective k-mer tables
# Change data for each run to match the reference genome
#%% Loading packages
import pandas as pd
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from imblearn.ensemble import BalancedRandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.metrics import confusion_matrix
from sklearn.metrics import matthews_corrcoef
import glob
import os
from sklearn.linear_model import LogisticRegression
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.cross_decomposition import PLSRegression
from sklearn.preprocessing import StandardScaler



#%% List of all files

files = []
for filepath in glob.iglob('D:/Master/K_mer_data/*_L_90.csv'):
    files.append(filepath)


    #%% Making automated pipeline

df_imb = pd.DataFrame(columns=("Precision", "Recall", "F1", "MCC", 
                           "Species", "Classifier", "Params", "Red")) # the dataframe that will house all metrics.


    
for file in files:
    data = pd.read_csv(file, index_col = 0)
        
    X = data.drop("Target", axis=1)
    y = data['Target']
    
    X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, random_state=42)
           
    # Imblearn random forest
        
    pipe = Pipeline([('scaler', StandardScaler()), ('classifier', BalancedRandomForestClassifier())])
        
        
    # define model
        
    param_grid = [
        {'classifier__n_estimators' : [300, 400, 500, 600, 700, 800, 900]}]
        
    # Create grid search object
        
    clf = GridSearchCV(pipe, param_grid = param_grid,
                          cv = 3, verbose=True, n_jobs=1)
    
     # Fit on data
        
    best_clf = clf.fit(X_train, y_train)
    pred = best_clf.predict(X_test)
    # calculate prediction
    precision_recall_fscore = precision_recall_fscore_support(y_test, pred, average='binary')
    MCC = matthews_corrcoef(y_test, pred)
    species = os.path.basename(file)
        
    df_i = pd.DataFrame([[precision_recall_fscore[0], precision_recall_fscore[1],
                            precision_recall_fscore[2], MCC, species, "imblearn",
                            best_clf.best_params_]])
    
    df_imb = df_imb.append(df_i)
        
#%% Making automated pipeline

df = pd.DataFrame(columns=("Precision", "Recall", "F1", "MCC", 
                           "Species", "Classifier", "Params", "Red")) # the dataframe that will house all metrics.
files = ["D:/Master/K_mer_data/Bacillus_subtilis_L_90.csv", 
         "D:/Master/K_mer_data/Campylobacter_jejuni_L_90.csv",
         "D:/Master/K_mer_data/Caulobacter_vibriodes_L_90.csv",
         "D:/Master/K_mer_data/E._coli_O156H7_sakai_L_90.csv"
         ]

reduction = [0, 500, 1000, 1500] # Number of features to reduce by
    
for file in files:
    data = pd.read_csv(file, index_col = 0)
    for red in reduction:
        
        X = data.drop("Target", axis=1)
        X = X.loc[:, (X).sum() >= red] 
        y = data['Target']
    
        X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, random_state=42)
           
    # Imblearn random forest
        
        pipe = Pipeline([('scaler', StandardScaler()), ('classifier', BalancedRandomForestClassifier())])
        
        
        # define model
        
        param_grid = [
            {'classifier__n_estimators' : [300, 400, 500, 600, 700, 800, 900]}]
        
        # Create grid search object
        
        clf = GridSearchCV(pipe, param_grid = param_grid,
                           cv = 3, verbose=True, n_jobs=1)
        
        # Fit on data
        
        best_clf = clf.fit(X_train, y_train)
        pred = best_clf.predict(X_test)
        # calculate prediction
        precision_recall_fscore = precision_recall_fscore_support(y_test, pred, average='binary')
        MCC = matthews_corrcoef(y_test, pred)
        species = os.path.basename(file)
        
        df_i = pd.DataFrame([[precision_recall_fscore[0], precision_recall_fscore[1],
                              precision_recall_fscore[2], MCC, species, "imblearn",
                              best_clf.best_params_, red]])
        df = df.append(df_i)
        
    
    # Logistic regression
        param_grid = [
            {'classifier' : [LogisticRegression()],
            'classifier__C' : np.logspace(-2, 2, 5)}]
        
        # Create grid search object
        
        clf = GridSearchCV(pipe, param_grid = param_grid, cv = 3, verbose=True,
                           n_jobs=1)
        
        # Fit on data
        
        best_clf = clf.fit(X_train, y_train)
        
        pred = (np.round(best_clf.predict_proba(X_test)))[:, 1]
        # calculate prediction
        precision_recall_fscore = precision_recall_fscore_support(y_test, pred, average='binary')
        MCC = matthews_corrcoef(y_test, pred)
        species = os.path.basename(file)
        
        df_i = pd.DataFrame([[precision_recall_fscore[0], precision_recall_fscore[1], 
                              precision_recall_fscore[2], MCC, 
                              species, "logistic regression",
                              best_clf.best_params_, red]])
        df = df.append(df_i)
        
      # KNN classifier  
        
        param_grid = [{'classifier' : [KNeighborsClassifier()],
            'classifier__p': [1, 2],
            'classifier__n_neighbors': [5, 10]}]
        
        # Create grid search object
        
        clf = GridSearchCV(pipe, param_grid = param_grid,
                           cv = 3, verbose=True, n_jobs=1)
        
        # Fit on data
        
        best_clf = clf.fit(X_train, y_train)
        pred = best_clf.predict(X_test)
        precision_recall_fscore = precision_recall_fscore_support(y_test, pred, average='binary')
        MCC = matthews_corrcoef(y_test, pred)
        species = os.path.basename(file)
        
        df_i = pd.DataFrame([[precision_recall_fscore[0], precision_recall_fscore[1], 
                                     precision_recall_fscore[2], MCC, species, "knn",
                                     best_clf.best_params_, red]])
        df = df.append(df_i)
        
            
df.to_csv("D:/Master/classifiers_results.csv", sep='\t')




