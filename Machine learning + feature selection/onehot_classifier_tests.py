# -*- coding: utf-8 -*-
"""
Created on Sat Apr 23 16:13:56 2022

@author: Trude
"""

# Code for creating one hot encoded feature matrix from the upstream
# table
import pandas as pd
from sklearn.metrics import precision_recall_fscore_support
from sklearn.model_selection import train_test_split
from imblearn.ensemble import BalancedRandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.metrics import matthews_corrcoef
import os
from sklearn.linear_model import LogisticRegression
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.cross_decomposition import PLSRegression
from sklearn.preprocessing import StandardScaler



#%%
data1 = pd.read_csv("D:/Master/Upstream_table_L_90.csv", index_col = 0)


#%% Making automated pipeline

df = pd.DataFrame(columns=("Precision", "Recall", "F1", "MCC", 
                           "Species", "Classifier", "Params", "Red")) # the dataframe that will house all metrics.
genomes = ['Campylobacter_jejuni', 'E._coli_O156H7_sakai', 
           'Bacillus_subtilis', 
           'Caulobacter_vibriodes']

reduction = [0, 500, 1000, 1500] # Number of features to reduce by

for file in genomes:
    data = data1[data1['Species'] == file]
    idx = data.loc[pd.isna(data["Sequence"]), :].index
    seq = data["Sequence"].dropna(axis=0, how = 'all')
    byposition = seq.apply(lambda x:pd.Series(list(x)))
    res = pd.get_dummies(byposition)
    if sum(idx) == 0:
        y = data['Target']
    else:
        y = data['Target']
        r = y.pop(idx[0])
        
    path = f"D:/Master/Sequential_data/{file}.csv"    
    dff = res.assign(target = y)
    dff.to_csv(path)

#%%
    
for file in genomes:
    data = data1[data1['Species'] == file]
    idx = data.loc[pd.isna(data["Sequence"]), :].index
    seq = data["Sequence"].dropna(axis=0, how = 'all')
    byposition = seq.apply(lambda x:pd.Series(list(x)))
    res = pd.get_dummies(byposition)
    if sum(idx) == 0:
        y = data['Target']
    else:
        y = data['Target']
        r = y.pop(idx[0])
    
    for red in reduction:
        
        X = res.loc[:, (res).sum() >= red]     
        X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, random_state=42)
        
    # Imblearn random forest
        
        pipe = Pipeline([('scaler', StandardScaler()), ('classifier', BalancedRandomForestClassifier())])
        
        
        # define model
        
        param_grid = [
            {'classifier__n_estimators' : [300, 400, 500, 600, 700, 800, 900]}]
        
        # Create grid search object
        
        clf = GridSearchCV(pipe, param_grid = param_grid,
                           cv = 3, verbose=True, n_jobs=1)
        
        # Fit on data
        
        best_clf = clf.fit(X_train, y_train)
        pred = best_clf.predict(X_test)
        # calculate prediction
        precision_recall_fscore = precision_recall_fscore_support(y_test, pred, average='binary')
        MCC = matthews_corrcoef(y_test, pred)
        species = os.path.basename(file)
        
        df_i = pd.DataFrame([[precision_recall_fscore[0], precision_recall_fscore[1],
                              precision_recall_fscore[2], MCC, species, "imblearn",
                              best_clf.best_params_, red]])
        df = df.append(df_i)
        
    
    # Logistic regression
        param_grid = [
            {'classifier' : [LogisticRegression()],
            'classifier__C' : np.logspace(-2, 2, 5)}]
        
        # Create grid search object
        
        clf = GridSearchCV(pipe, param_grid = param_grid, cv = 3, verbose=True,
                           n_jobs=1)
        
        # Fit on data
        
        best_clf = clf.fit(X_train, y_train)
        
        pred = (np.round(best_clf.predict_proba(X_test)))[:, 1]
        # calculate prediction
        precision_recall_fscore = precision_recall_fscore_support(y_test, pred, average='binary')
        MCC = matthews_corrcoef(y_test, pred)
        species = os.path.basename(file)
        
        df_i = pd.DataFrame([[precision_recall_fscore[0], precision_recall_fscore[1], 
                              precision_recall_fscore[2], MCC, 
                              species, "logistic regression",
                              best_clf.best_params_, red]])
        df = df.append(df_i)
        
      # KNN classifier  
        
        param_grid = [{'classifier' : [KNeighborsClassifier()],
            'classifier__p': [1, 2],
            'classifier__n_neighbors': [5, 10]}]
        
        # Create grid search object
        
        clf = GridSearchCV(pipe, param_grid = param_grid,
                           cv = 3, verbose=True, n_jobs=1)
        
        # Fit on data
        
        best_clf = clf.fit(X_train, y_train)
        pred = best_clf.predict(X_test)
        precision_recall_fscore = precision_recall_fscore_support(y_test, pred, average='binary')
        MCC = matthews_corrcoef(y_test, pred)
        species = os.path.basename(file)
        
        df_i = pd.DataFrame([[precision_recall_fscore[0], precision_recall_fscore[1], 
                                     precision_recall_fscore[2], MCC, species, "knn",
                                     best_clf.best_params_, red]])
        df = df.append(df_i)
        
    # LDA + PLS
        
        # Fit on data
        
        y_hat_p = f"D:/Master/LDA/yhat/{file}{red}.csv"
        y_p = f"D:/Master/LDA/y/{file}{red}.csv"
        
        y_hat = pd.read_csv(y_hat_p)
        y_tr = pd.read_csv(y_p)
        
        precision_recall_fscore = precision_recall_fscore_support(y_tr["x"], y_hat["x"], average='binary')
        MCC = matthews_corrcoef(y_tr["x"], y_hat["x"])
        species = os.path.basename(file)
        
        df_i = pd.DataFrame([[precision_recall_fscore[0], precision_recall_fscore[1], 
                                     precision_recall_fscore[2], MCC, species, "PLS+LDA",
                                     best_clf.best_params_, red]])
        df = df.append(df_i)
       
        print(red, file, "complete")
    print("file complete", file)
            


#%% Onehot random forest classification for all genomes

rf_results = pd.DataFrame()
genomes = np.unique(data1["Species"])


for genome in genomes:
    data = data1[data1['Species'] == genome]
    idx = list(data.loc[pd.isna(data["Sequence"]), :].index)
    seq = data["Sequence"].dropna(axis=0, how = 'all')
    byposition = seq.apply(lambda x:pd.Series(list(x)))
    res = pd.get_dummies(byposition)
    
    
    if sum(idx) == 0:
        y = data['Target']
    else:
        y = data['Target']
        for ele in sorted(idx, reverse = True):
            del y[ele]

    X = res    
    X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y, random_state=42)
           
    # Imblearn random forest
        
    pipe = Pipeline([('scaler', StandardScaler()),
                     ('classifier', BalancedRandomForestClassifier())])
        
        
    # define model
        
    param_grid = [
    {'classifier__n_estimators' : [100, 200, 300, 400, 500, 600, 700, 800, 900]}]
        
    # Create grid search object
        
    clf = GridSearchCV(pipe, param_grid = param_grid,
                           cv = 3, verbose=True, n_jobs=1)
        
    # Fit on data
        
    best_clf = clf.fit(X_train, y_train)
    pred = best_clf.predict(X_test)
    # calculate prediction
    precision_recall_fscore = precision_recall_fscore_support(y_test, pred, average='binary')
    MCC = matthews_corrcoef(y_test, pred)
    species = os.path.basename(file)
        
    df_i = pd.DataFrame([[precision_recall_fscore[0], precision_recall_fscore[1],
                           precision_recall_fscore[2], MCC, genome,
                           best_clf.best_params_]])
    rf_results = rf_results.append(df_i)
    
    
#%% LDA runalone
    
df = pd.DataFrame()
for file in genomes:
     for red in reduction:
        
    
     # LDA + PLS
        
        # Fit on data
        
        y_hat_p = f"D:/Master/LDA/yhat/{file}{red}.csv"
        y_p = f"D:/Master/LDA/y/{file}{red}.csv"
        
        y_hat = pd.read_csv(y_hat_p)
        y_tr = pd.read_csv(y_p)
        
        precision_recall_fscore = precision_recall_fscore_support(y_tr["x"], y_hat["x"], average='binary')
        MCC = matthews_corrcoef(y_tr["x"], y_hat["x"])
        species = os.path.basename(file)
        
        df_i = pd.DataFrame([[precision_recall_fscore[0], precision_recall_fscore[1], 
                                     precision_recall_fscore[2], MCC, species, "PLS+LDA",
                                     best_clf.best_params_, red]])
        df = df.append(df_i)