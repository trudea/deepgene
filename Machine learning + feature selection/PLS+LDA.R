library(tidyverse)
library(microclass)
library(mpda)


files <- list.files("D:/Master/Sequential_data") # Created by one-hot encoding


for(i in files){
  set.seed(101) # Set Seed so that same sample can be reproduced in future also
  # Now Selecting 75% of data as sample from total 'n' rows of the data  
  read.csv(i) -> ups
  sample <- sample.int(n = nrow(ups), size = floor(.75*nrow(ups)), replace = F)
  train <- ups[sample, ]
  test  <- ups[-sample, ]
  
  train_y <- factor(train$target)
  train_X <- as.matrix(subset(train, select = -c(X, target)))
  
  test_y <- factor(test$target)
  test_X <- as.matrix(subset(test, select = -c(X, target)))
  
  # Estimates number of components for PLS. This is slow and may be skipped
  # in the initial testing, just choosing a number of components manually
  #lst <- pdaDim(train_y, unlist(train_X), reg = 0.5, prior = c(0.95, 0.05), max.dim = 20, n.seg = 100)
  
  # The PLS+LDA fitting. Choose prior probabilities, or NULL which is empirical prior
  max.dim <- 20  #lst$Dimension  # or just 10 or 20 ...
  pda.fit <- pda(train_y, train_X, prior = NULL, max.dim = max.dim)
  
  # Predicting, re-using training data
  p.lst <- predict(pda.fit, test_X)
  y.hat <- p.lst[[max.dim]]$Classifications
  
  # confusion matrix and accuracy
  ct <- table(test_y, y.hat)
  acc <- (ct[1,1] + ct[2,2]) / sum(ct) 
  
  path_y_hat <- paste("D:/Master/LDA/yhat/", basename(i), sep="")
  path_y <- paste("D:/Master/LDA/y/",basename(i), sep="")
  
  write.csv(y.hat, file = path_y_hat)
  write.csv(test_y, file = path_y)
}
