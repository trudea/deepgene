#!/bin/bash

#SBATCH --nodes=1                        # We always use 1 node
#SBATCH --ntasks=1                       # The number of threads reserved
#SBATCH --mem=10G                        # The amount of memory reserved
#SBATCH --partition=smallmem             # For < 100GB use smallmem, for >100GB use hugemem
#SBATCH --time=01:00:00                  # Runs for maximum this time
#SBATCH --job-name=prodigal               # Sensible name for the job
#SBATCH --output=prodigal_%j.log          # Logfile output here


##############
### Settings
###
dir=deepgene/fna_prokaryotes
genome_files=$(ls $dir | grep .fna)
out_dir=$SCRATCH/prodigal/

####################################
### Testing if $out_dir exists
### If yes, delete all files in it
### If no, create it
###
if [ ! -d $out_dir ]
then
  mkdir $out_dir
fi

############
### Looping
###
for i in $genome_files;
do
  singularity exec /cvmfs/singularity.galaxyproject.org/p/r/prodigal:2.6.3--h779adbc_3 prodigal \
  -p single -f gff -a "${out_dir}${i}.ffn" -d "${out_dir}${i}.faa" -o "${out_dir}${i}.gff" -i "${dir}/${i}"
done


